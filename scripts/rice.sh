#!/bin/bash

i3-msg workspace 3

alacritty -e sh -c "neofetch; exec $SHELL" --hold split_v &
sleep 0.3
alacritty &
sleep 0.3
i3-msg split v
alacritty -e sh -c "htop; exec $SHELL" &
sleep 1
i3-msg split h

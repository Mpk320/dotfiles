#!/bin/sh

# Set your monitor names
PRIMARY="DP-4"
SECONDARY_LEFT="DP-2"
SECONDARY_RIGHT="DP-0"

# Set primary monitor
xrandr --output $PRIMARY --primary

# Set position of secondary monitors
xrandr --output $SECONDARY_LEFT --left-of $PRIMARY
xrandr --output $SECONDARY_RIGHT --right-of $PRIMARY

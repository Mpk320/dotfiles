#!/bin/bash

killall polybar

# Launch Polybar for DP-0
unset MONITOR
MONITOR=DP-0 polybar --reload main &

# Launch Polybar for DP-2
unset MONITOR
MONITOR=DP-2 polybar --reload main &

# Launch Polybar for DP-4
unset MONITOR
MONITOR=DP-4 polybar --reload main &
